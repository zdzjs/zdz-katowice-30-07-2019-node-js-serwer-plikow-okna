//dodajemy wymagania biblioteki express
const e = require('express');
//tworzymy dowiązanie do obiekru Router() biblioteki express
//Router() pozwala na obsługę zapytania (żądania) klienta 
//w zależnościo od zawartości parametrów połączenia od klienta
const router = e.Router();

//wskazujemy, że dołączamy naszą bibliotekę uwuload.js
//znajdującą się w ścieżce ./uwuload
router.use('/uwuload', require('./uwuload'));
//exportujemy zasób router
module.exports = router;
