const e = require('express');
const router = e.Router();
const multer  = require('multer');
const fs = require('fs');
const path = require('path');
//poniższy kod zapisany został w standardzie ES6, 
//który umożliwia odczyt danych wejściowych z tablic lub obiektów
//jako niezależnych zmiennych
//więcej: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
//https://stackoverflow.com/questions/41058569/what-is-the-difference-between-const-and-const-in-javascript
const { promisify } = require('util');

//stała przechowuje format zachowywania plików na dysku serwera
//które zostaną przesłane od klienta;
//m. in. mamy ustalone miejsce przechowywania (folder projektu/uploads/)
//oraz jaką nazwę plik ma przyjąć w przypadku zapis (brak właściwości
//filename powoduje nadawanie losowych nazw)
//więcej: https://www.npmjs.com/package/multer
const magazyn = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
//dodanie mapy przechowywania (stała magazyn) do tworzonego obiektu
//upload
const upload = multer({ storage: magazyn });
//utworzenie 'Obietnicy'. Jest to jeden z mechanizmów asynchronicznego przetwarzania
//kodu/instrukcji - całych bloków, np. funkcji - z mechanizmem obsługi
//wyjątków; generalnie Promise może zwracać jedno zachowanie jeżeli wykona
//się poprawnie oraz inne, gdy wykona się z błędem. "Zachowanie" w tym
//kontekście to po prostu pojedyncza zmienna, instrukcja lub cała funkcja 
//NAJWAŻNIEJSZE  - wszystko wykonuje się asynchronicznie, tj. po utworzeniu
//obiektu Promise czas jego działania nie jest dla nas znany a tym samym
//domyślnie nie mamy pewności, kiedy z wyniku działania tego obiektu będziemy
//mogli korzystać
//więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Promise
const readDirPromise = promisify(fs.readdir);
const unlinkPromise = promisify(fs.unlink);
const statPromise = promisify(fs.stat);
const renamePromise = promisify(fs.rename);

/*
	Poniższy zapis to tzw. zapis łańcuchowy kodu, gdzie po kolejnych
	spójnikach (kropki) podajemy kolejne metody obiektu zwróconego
	przez metodę route() obiektu router.
	Kolejne metody pozwalają na obsługę kolejnych zdarzeń jakie mogą
	wystąpić podczas obsługi żądania klienta do naszego serwera
*/
router.route('/')
	//kod poniższej metody wykona się, gdy klient wyśle żądanie GET
	//słowo async przez deklaracją funkcji powoduje jej asynchroniczne 
	//wykonywanie (kod JavaScript nie będzie czekał na jej wynik, tj.
	//zachowanie nieblokowalne - non-blocking function
  .get(async (req, res) => {
	  //słowo await nakazuje parserowi JavaScript poczekać na wynik działania
	  //obiektu, którego domyślnym zachowaniem jest działanie asynchroniczne
	  //tym samym tworzymy kod blokujący (blocking function)
    const files = await readDirPromise('uploads/');
    //poniższy obiekt res utworzy obiekt JSON (w celu odpowiedzi do 
	//klienta) z tablica zawierającą nazwy wszystkich plików w katalogu
	//uploads
	res.json({
      code: 200,
      files
    })
  })
	//ten kody wykona się jeżeli dostaniemy polecenie POST od klienta
	//w skrócie - klient wysłał do nas pliki do załadowania na serwer
  .post(upload.array('files'), (req, res) => {
    //odpowiedź będzie zawierać informację jaki plik(i) został przesłany
	//na serwer
	res.json({
      code: 200,
      files: req.files
    })
  })
	//obsługa polecenie DELETE od klienta; w naszym wypadku ma za zadanie
	//wyrzucić wszystkie pliki jakie dostępne są w katalogu uploads
  .delete(async (req, res) => {
    const files = await readDirPromise('uploads/');
	//bardzie elegancki zapis używanej dotychczas metody forEach
    for(file of files) {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', file))
    }
	//odpowiedź będzie zawierać informację, że wszystkie pliki na serwerze zostały
	//skasowane
    res.json({
      code: 200,
      message: 'Files removed.'
    })
  });
//zachowanie serwera gdy podamy nazwę pliku 
//np. http://localhost:1337/plik_do_skasowania.txt

router.route('/:filename')
//operacja wykonywana gdy otrzymamy polecenie GET
/**
	Poniżej znajduje się DOKŁADNIE TEN SAM KOD w trzech różnych wersjach. Każda z nich wykona dla nas dokładnie
	to samo zadanie oraz wyświetli ten sam wynik.
*/

/**
	Pierwsza wersja kodu zakłada standardowe użycie funkcji fs.stat zamiast opakowania Promise. W ramach
	trzeciego parametru podajemy funkcje tzw. callback, której wykonanie jest GWARANTOWANE na sam koniec
	kodu funkcji jej wywołującej. Innymi słowy fs.stat, gdy wykona już WSZYSTKIE swoje zadania wywoła podaną
	przez nas funkcję. To swego rodzaju asynchroniczność w JavaScript (wywołujemy funkcję i nie czekamy na jej wynik - 
	jej wynik będzie natychmiast umieszony np. w callback. Należy zauważyć, że zamiast nazwy funkcji (np. wykonajMnieNakoniec)
	możemy po prostu napisać własną funkcję anonimową, która ma się wykonać na zakończenie działania pierwszej funkcji (fs.stat).
	W tym wypadku tak właśnie zrobiono
*/
/*.get( (req, res) => {
	var tStart=process.hrtime();
	fs.stat(path.join(process.cwd(), 'uploads/', req.params.filename), (blad,statPliku) => {
		if (statPliku) res.sendFile(path.join(process.cwd(), 'uploads/', req.params.filename));
		else 
			res.json({
		  //plik nie został znaleziony, zamiast 200 wysyłamy kod HTTP 404
			code: 404,
			message: 'Not Found'
		  })
		  console.log("Koniec obsługi, callback",process.hrtime(tStart));
	});
	console.log("Koniec obsługi",process.hrtime(tStart));
  }) */
  /**
	Kolejne rozwiązanie, korzystającej z czystej implementacji obiektów Promise (Obietnica). 
	Obiekt ten z założenia wykonuje się asynchronicznie do reszty kodu (w teorii nie blokując wywołania reszty
	skryptu). Sam w sobie zawiera obsługę kodu warunkowego try/catch. Chociaż w klasycznej postaci kod
	Promise wygląda nieco inaczej, poniżej prezentowana jest jego nowa, odświeżona wersja. Nas będą interesować
	dwie metody:
	then - jako parametr przyjmuje funkcję, która ma wykonać się w przypadku, gdy kod Obietnicy zakończy się
	powodzeniem; W naszym wypadku to funkcja, która ma za zadanie przesłać do przeglądarki znaleziony plik
	catch - może przyjąć zmienną-obiekt, która będzie zawierać informacje o niepowodzeniu działania obiektu
	ponadto możemy wykonać dowolny kod na okoliczność wystąpienia błędu ( w tym wypadku przesyłamy błąd o 
	nie znalezieniu obrazu do przeglądarki)
	
	Poniżej wykorzystano fukcję mierzenia czasu wykonania poszczególnych zadań - https://blog.abelotech.com/posts/measure-execution-time-nodejs-javascript/
	Wersja podająca osobno części całkowite sekund oraz milisekund z dużą precyzcją
  */
/*.get((req, res) => {
	var tStart=process.hrtime();
	statPromise(path.join(process.cwd(), 'uploads/', req.params.filename))
	.then( () => {
		  res.sendFile(path.join(process.cwd(), 'uploads/', req.params.filename));
		  console.log("Koniec obsługi, sukces",process.hrtime(tStart));
	})
    .catch( (error) => {
      res.json({
		  //plik nie został znaleziony, zamiast 200 wysyłamy kod HTTP 404
        code: 404,
        message: 'Not Found'
      })
	  console.log("Koniec obsługi, porażka",process.hrtime(tStart));
    })
	console.log("Koniec obsługi",process.hrtime(tStart));
  }) */
  /**
	Ostatnie rozwiązanie, korzystające z mechanizmu async/await. 
	Słowo async powoduje, że funkcja tworzona w get będzie wykonywać się asynchronicznie
	do całego naszego kodu. W zasadzie jest ona opakowana w obiekt Promise.
	
	Następnie tworzymy synchroniczny blok try/catch. W nim to tworzymy element Promise 
	tak jak w przykładzie powyżej. To co odórżnia oba kody to eliminacja metod
	then oraz catch na końcu obiektu. Zamiast tego przez wywołaniem samego obiektu
	wstawiamy słowo await. Powoduje ono, że kod naszej aplikacji będzie w tym miejscu blokowany i 
	skrypt zaczeka aż pojawi się wynik z asynchronicznego obiektu.
	Następne linie kodu wykonają się dopiero po uzyskaniu wyniku. Cokolwiek w ciele try spowoduje błąd
	automatycznie przekieruje nas do catch. W poniższym przypadku właściwie tylko niepowodzenie Promise
	może przenieść nas do catch...
	
	które wykona się w przypadku, gdy pobierany plik nie istnieje. 
	
	Chociaż wydawać by się mogło, że ta konstrukcja powinna być wolniejsza od porzednio założonej (dwa obiekty Promise,
	dodatkowa pętla try/catch i wewnętrzne resolve/reject w każdy z Promise) to sumarycznie rozwiązanie to jest
	NAJBARDZIEJ OPTYMALNE (względem czasu wykonania) - przynajmniej na bibliotece V8. Wspomniany parser 
	optymalizuje wykonywanie operacji async/await poprzez wewnętrzną zamianę wywoływania Promise z 2 na 1. Ponadto
	sam mechanizm eleiminuje dwa oczekiwania w sekwencji (microticks), przez co kod wykonuje się minimalnie szybciej
	niż przy zastosowaniu klasycznej wersji Promise.
	
	Więcej informacji można znaleźć:
	https://v8.dev/blog/fast-async
	
	Więcej o callback, Promise oraz async/await:
	https://itnext.io/javascripts-async-await-versus-promise-the-great-debate-6308cb2e10b3
  */
  .get(async (req, res) => {
	  var tStart=process.hrtime();
    try {
	 
		  await statPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
		  res.sendFile(path.join(process.cwd(), 'uploads/', req.params.filename));
		  console.log("Koniec obsługi, sukces",process.hrtime(tStart));
    } catch (error) {
      res.json({
		  //plik nie został znaleziony, zamiast 200 wysyłamy kod HTTP 404
        code: 404,
        message: 'Not Found'
      })
	  console.log("Koniec obsługi, porażka",process.hrtime(tStart));
    }
	console.log("Koniec obsługi",process.hrtime(tStart));
  }) 
  //operacja niezbędna do obsłużenia żądania PUT, służącego do zmian elementów/informacji na serwerze
  //można tego typu operacje wykonać w get, jednak bardziej elegancko jest rozwiązać to poprzez stworzone 
  //pod to żądanie.
  //PONIŻSZY KOD MOŻNA ZOPTYMALIZOWAĆ BAZUJĄC NA POWYŻSZYCH ROZWAŻANIACH DOTYCZĄCYCH GET
  .put((req,res) => {
	  renamePromise(path.join(process.cwd(), 'uploads/', req.params.filename),path.join(process.cwd(), 'uploads/', req.body.fileName))
	  .then( () => {
		  res.json({code:200, message: 'Zmieniono!'})
	  })
	  .catch( (err) => {
		  res.json({
			code: 404,
			message: `Pliku ${req.params.filename} nie znaleziono`
		  })
	  })
  })
  //operacja wykonywana gdy otrzymamy polecenie DELETE
  .delete(async (req, res) => {
    try {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
      res.json({
        code: 200,
        message: `${req.params.filename} removed.`
      })
    } catch (error) {
      res.json({
        code: 404,
        message: 'Not Found'
      })
    }
  })
  

module.exports = router;
